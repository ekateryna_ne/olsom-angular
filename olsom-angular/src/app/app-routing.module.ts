import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { MainStylesPageComponent } from './pages/main-styles-page/main-styles-page.component';
import { PropertiesPageComponent } from './pages/properties-page/properties-page.component';

const routes: Routes = [
  { path: '', component: MainStylesPageComponent },
  { path: 'properties', component: PropertiesPageComponent },
  { path: 'properties:view=line', component: PropertiesPageComponent },
  { path: 'index', component: IndexPageComponent },
  { path: 'login', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
