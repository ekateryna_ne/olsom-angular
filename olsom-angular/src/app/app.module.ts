import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { HeaderComponent } from './core/layout/header/header.component';
import { FooterComponent } from './core/layout/footer/footer.component';
import { NavigationTreeComponent } from './core/layout/navigation-tree/navigation-tree.component';
import { MenuComponent } from './core/layout/menu/menu.component';
import { TabsComponent } from './core/components/tabs/tabs.component';
import { BreadcrumbsComponent } from './core/components/breadcrumbs/breadcrumbs.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DropdownComponent } from './core/components/dropdown/dropdown.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { PropertiesPageComponent } from './pages/properties-page/properties-page.component';
import { FiltersComponent } from './core/components/filters/filters.component';
import { PropertiesListComponent } from './core/components/properties-list/properties-list.component';
import { PropertiesLineComponent } from './core/components/properties-list/properties-line/properties-line.component';
import { PropertiesTileComponent } from './core/components/properties-list/properties-tile/properties-tile.component';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { DialogWindowComponent } from './core/components/dialog-window/dialog-window.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { MenuModule } from '@progress/kendo-angular-menu';
import { HeaderShortComponent } from './core/layout/header-short/header-short.component';
import { MainStylesPageComponent } from './pages/main-styles-page/main-styles-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavigationTreeComponent,
    MenuComponent,
    TabsComponent,
    BreadcrumbsComponent,
    DropdownComponent,
    IndexPageComponent,
    LoginPageComponent,
    PropertiesPageComponent,
    FiltersComponent,
    PropertiesListComponent,
    PropertiesLineComponent,
    PropertiesTileComponent,
    DialogWindowComponent,
    HeaderShortComponent,
    MainStylesPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    DialogsModule,
    LayoutModule,
    MenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
