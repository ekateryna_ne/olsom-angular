import {style, animate, trigger, state, transition} from '@angular/animations';

export const showHideTrigger = trigger('showHideTrigger', [
    state('show', style({
        height: '*',
        overflow: 'hidden'
    })),

    transition('void => show', [
        style({
            height: '0'
        }),
        animate('0.4s ease-out', style({
            height: '*'
        }))
    ]),

    transition('show => void', [
        style({
            height: '*'
        }),
        animate('0.4s ease-out', style({
            height: '0'
        }))
    ])
]);
