import {Component, OnInit} from '@angular/core';
import {showHideTrigger} from '../../../../animations';

@Component({
    selector: 'app-properties-tile',
    templateUrl: './properties-tile.component.html',
    animations: [showHideTrigger]
})
export class PropertiesTileComponent implements OnInit {
    propertieItemShow: boolean = true;

    constructor() {
    }

    ngOnInit() {
    }

}
