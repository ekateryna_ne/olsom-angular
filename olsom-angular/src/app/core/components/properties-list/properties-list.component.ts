import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-properties-list',
    templateUrl: './properties-list.component.html'
})

export class PropertiesListComponent implements OnInit {
    viewMode = 'tile';

    constructor() {
    }

    ngOnInit() {
    }
}
