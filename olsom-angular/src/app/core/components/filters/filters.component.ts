import {Component, OnInit, HostBinding} from '@angular/core';

import {showHideTrigger} from 'src/app/animations';

// import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.scss'],
    animations: [showHideTrigger]
})

export class FiltersComponent implements OnInit {
    public showFilters: boolean = true;
    public buttonName: any = 'Hide filters';

    constructor() {
    }

    ngOnInit() {
    }

    toggleFilters() {
        this.showFilters = !this.showFilters;

        if (this.showFilters) {
            this.buttonName = 'Hide filters';
        } else {
            this.buttonName = 'Show filters';
        }
    }
}
