import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-dialog-window',
    templateUrl: './dialog-window.component.html'
})

export class DialogWindowComponent implements OnInit {
    public opened = false;
    public dataSaved = false;

    constructor() {
    }

    ngOnInit() {
    }


    public close() {
        this.opened = false;
    }

    public open() {
        this.opened = true;
    }

    public submit() {
        this.dataSaved = true;
        this.close();
    }
}
