import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
    public listItems: Array<string> = [
        'Baseball', 'Basketball', 'Cricket', 'Field Hockey',
        'Football', 'Table Tennis', 'Tennis', 'Volleyball'
    ];

    public value = ['Basketball', 'Cricket'];
  constructor() { }

  ngOnInit() {
  }
}
