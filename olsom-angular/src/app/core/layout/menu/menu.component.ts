import {Component, OnInit} from '@angular/core';
import {items} from '../../../core/layout/menu/items';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html'
})

export class MenuComponent {
    public items: any[] = items;

    /* public listMenuItems: Array<string> = [
         'production-capability',
         'equipment',
         'routings',
         'calendars',
         'users',
         'schedule',
         'tools',
         'customers',
         'production',
         'packing',
         'shipping',
         'matrix-qualifications',
         'product-definition',
         'materials-bom',
         'part-families',
         'models-products',
         'operations',
         'production-counts',
         'scrap-reasons',
         'downtime-reasons',
         'jobs',
         'etalon-jobs',
         'customer-orders',
         'sequence-control'
     ];*/
}
